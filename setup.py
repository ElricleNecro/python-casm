#!/usr/bin/env python
# coding: utf-8

from setuptools import setup, find_packages

setup(
    name='casm',
    version="0.0.2a0",
    description="A little compiler for a personal assembly and vm.",
    author="Guillaume Plum",
    author_email="guiplum@gmail.com",
    url="https://www.gitlab.com/ElricleNecro/python-casm",
    license="GPLv3",
    packages=find_packages(),
    entry_points={
        'console_scripts': ['casm = casm.__main__:main'],
    },
    test_suite='test',
    provides=['casm'],
)
