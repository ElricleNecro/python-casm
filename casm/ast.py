# coding: utf-8

"""AST related module.

:author: Guillaume Plum <guillaume.plum@zaclys.net>
"""


from dataclasses import dataclass, field
from enum import Enum
from typing import List


__all__ = [
    "Parameter",
    "ASTNode",
]


@dataclass
class Parameter:
    """Represent a command parameter."""

    is_imm: bool = False
    is_hex: bool = False
    value: str = ""


@dataclass
class ASTNode:
    """Node of the AST."""

    stmt: str
    args: List[Parameter] = field(default_factory=list)
