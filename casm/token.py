# coding: utf-8

"""Token related module.

:author: Guillaume Plum <guillaume.plum@zaclys.net>
"""

from __future__ import annotations

from dataclasses import dataclass
from enum import Enum
from re import fullmatch


__all__ = [
    "Token",
    "TokenType"
]


class TokenType(Enum):
    """Token type."""

    NEW_LINE   = r"\n"
    WHITESPACE = r"\s"
    CHARACTERS = r"[a-zA-Z_]"
    DELIMITERS = r","
    NUMBERS    = r"[\-0-9]"
    OPERATORS  = r"[$#\[\]]"
    COMMENTS   = r";"

    @staticmethod
    def token(char: str) -> TokenType:
        """Instanciate the correct TokenType enum for the given character.

        :param char: Character to parse.
        :type char: str
        :return: The corresponding enum.
        :rettype: TokenType
        """
        for tt in TokenType:
            if fullmatch(tt.value, char):
                return tt

        raise ValueError(f"Character '{char}' did not match any token.")


@dataclass(init=False)
class Token:
    """Represent a token."""

    type: TokenType
    value: str

    def __init__(self, char: str):
        """Constructor."""
        self.type: TokenType = TokenType.token(char)
        self.value: str = char
