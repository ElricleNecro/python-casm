#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The comiler utilities module.

:author: Guillaume Plum <guillaume.plum@zaclys.net>
"""

from __future__ import annotations

from enum import Enum, auto
from typing import Callable, Dict, List

from .ast import ASTNode, Parameter
from .token import Token, TokenType


__all__ = [
    "UnexpectedState",
    "StateMachine",
]


class StateMachineStatus(Enum):
    """Which state the state machine is in."""

    CMD_NAME = auto()
    PARAMETER = auto()
    COMMENT = auto()


class UnexpectedState(Exception):
    """Exception raised in the case of an unexpected state of the state machine for the current token."""

    def __init__(self, state: StateMachineStatus, message: str):
        """Constructor."""
        self._state = state
        self._message = message

    def __repr__(self) -> str:
        """Representation of the exception."""
        return f"{self._state.name}: {self._message}"

    def __str__(self) -> str:
        """Exception as a string."""
        return f"{self._state.name}: {self._message}"


class StateMachine:
    """Parse a stream of Token into an AST."""

    def __init__(self):
        """Constructor.

        :param stream: List of token to parse into an AST.
        :type param: List of :class`Token`.
        """
        #  self._stream: List[Token] = stream
        self._ast: List[ASTNode] = list()
        self._status: StateMachineStatus = StateMachineStatus.CMD_NAME
        self._cur: ASTNode = ASTNode("")
        self._switch: Dict[TokenType, Callable[[Token], None]] = dict()

        self._switch[TokenType.NEW_LINE] = self._new_line
        self._switch[TokenType.WHITESPACE] = self._whitespace
        self._switch[TokenType.CHARACTERS] = self._characters_or_number
        self._switch[TokenType.DELIMITERS] = self._delimiter
        self._switch[TokenType.NUMBERS] = self._characters_or_number
        self._switch[TokenType.OPERATORS] = self._operator
        self._switch[TokenType.COMMENTS] = self._comments

    def _new_line(self, token: Token) -> None:
        """Found a new line, the command is over, so add a new node.

        If it is an empty line, ignore it.
        """
        if self._cur is None or self._cur.stmt == "":
            return

        self._ast.append(self._cur)
        self._cur = ASTNode("")
        self._status = StateMachineStatus.CMD_NAME

    def _characters_or_number(self, token: Token) -> None:
        """Found a character or a number, so push to the argument or command name."""
        if self._status is StateMachineStatus.COMMENT:
            return
        elif self._status is StateMachineStatus.CMD_NAME:
            self._cur.stmt += token.value
        elif self._status is StateMachineStatus.PARAMETER:
            self._cur.args[-1].value += token.value
        else:
            raise UnexpectedState(self._status, "Parsing a number/character.")

    def _delimiter(self, token: Token) -> None:
        """Found a delimiter, so add a new parameter to the command."""
        if self._status is StateMachineStatus.COMMENT:
            return
        elif self._status is StateMachineStatus.PARAMETER and self._cur.args[-1].value != "":
            self._cur.args.append(Parameter())
        else:
            raise UnexpectedState(self._status, f"Found a delimiter '{token.value}' when expecting something else.")

    def _comments(self, token: Token) -> None:
        """We are going into COMMENT state."""
        self._status = StateMachineStatus.COMMENT

    def _whitespace(self, token: Token) -> None:
        """Ignoring almost all spaces, except when finishing a command name."""
        if self._status is StateMachineStatus.CMD_NAME:
            self._status = StateMachineStatus.PARAMETER
            self._cur.args.append(Parameter())

    def _operator(self, token: Token) -> None:
        """Set conveninet flags for the current parameter."""
        if self._status is not StateMachineStatus.PARAMETER:
            raise UnexpectedState(self._status, f"Flags ('{TokenType.OPERATORS.value}') are only valid in command parameters.")

        if token.value == '#':
            self._cur.args[-1].is_imm = True
        elif token.value == '$':
            self._cur.args[-1].is_hex = True

    def parse(self, stream: List[Token]) -> List[ASTNode]:
        """Create the abstract syntax tree from the stream."""
        self._ast = list()
        self._cur = ASTNode("")

        for token in stream:
            self._switch[token.type](token)

        self._new_line(None)

        return self._ast
