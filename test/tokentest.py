# coding: utf-8

"""Testing the Token dataclass.

:author: Guillaume Plum <guillaume.plum@zaclys.net>
"""


from unittest import TestCase

from casm.token import Token, TokenType


class TokenTest(TestCase):
    """Unit test for the Token dataclass."""

    def test_token_new_line(self):
        """Test the NEW_LINE enum for the `Token` constructor."""
        token = Token('\n')
        self.assertTrue(token.value == '\n')
        self.assertTrue(token.type == TokenType.NEW_LINE)

    def test_token_whitespace(self):
        """Test the WHITESPACE enum for the `Token` constructor."""
        token = Token(' ')
        self.assertTrue(token.value == ' ')
        self.assertTrue(token.type == TokenType.WHITESPACE)

        token = Token('\t')
        self.assertTrue(token.value == '\t')
        self.assertTrue(token.type == TokenType.WHITESPACE)

    def test_token_characters(self):
        """Test the CHARACTERS enum for the `Token` constructor."""
        for c in "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ":
            token = Token(c)
            self.assertTrue(token.value == c)
            self.assertTrue(token.type == TokenType.CHARACTERS)

    def test_token_delimiters(self):
        """Test the DELIMITERS enum for the `Token` constructor."""
        token = Token(',')
        self.assertTrue(token.value == ',')
        self.assertTrue(token.type == TokenType.DELIMITERS)

    def test_token_numbers(self):
        """Test the NUMBERS enum for the `Token` constructor."""
        for n in "0123456789":
            token = Token(n)
            self.assertTrue(token.value == n)
            self.assertTrue(token.type == TokenType.NUMBERS)

    def test_token_operators(self):
        """Test the OPERATORS enum for the `Token` constructor."""
        for o in "$#[]":
            token = Token(o)
            self.assertTrue(token.value == o)
            self.assertTrue(token.type == TokenType.OPERATORS)

    def test_token_comments(self):
        """Test the COMMENTS enum for the `Token` constructor."""
        token = Token(';')
        self.assertTrue(token.value == ';')
        self.assertTrue(token.type == TokenType.COMMENTS)
