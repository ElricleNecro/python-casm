# coding: utf-8

"""Testing the ASTNode dataclass.

:author: Guillaume Plum <guillaume.plum@zaclys.net>
"""


from unittest import TestCase

from casm.ast import ASTNode


class ASTNodeTest(TestCase):
    """Unit test for the ASTNode dataclass."""

    def test_astnode_empty(self):
        """Test the constructor for the `ASTNode` class."""
        node = ASTNode("")
        self.assertTrue(node.stmt == "")
        self.assertTrue(node.args == [])
