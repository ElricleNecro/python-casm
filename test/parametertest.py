# coding: utf-8

"""Testing the Parameter dataclass.

:author: Guillaume Plum <guillaume.plum@zaclys.net>
"""


from unittest import TestCase

from casm.ast import Parameter


class ParameterTest(TestCase):
    """Unit test for the Parameter dataclass."""

    def test_astnode_empty(self):
        """Test the constructor for the `Parameter` class."""
        param = Parameter()

        self.assertFalse(param.is_imm)
        self.assertFalse(param.is_hex)
        self.assertTrue(param.value == "")
