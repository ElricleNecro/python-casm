# coding: utf-8

"""Testing the TokenType.token method.

:author: Guillaume Plum <guillaume.plum@zaclys.net>
"""


from unittest import TestCase

from casm.token import TokenType


class TokenTypeTest(TestCase):
    """Unit test for the TokenType enum."""

    def test_token_new_line(self):
        """Test the NEW_LINE enum for the `TokenType.token` method."""
        self.assertTrue(TokenType.token('\n') is TokenType.NEW_LINE)

    def test_token_whitespace(self):
        """Test the WHITESPACE enum for the `TokenType.token` method."""
        self.assertTrue(TokenType.token(' ') is TokenType.WHITESPACE)
        self.assertTrue(TokenType.token('\t') is TokenType.WHITESPACE)

    def test_token_characters(self):
        """Test the CHARACTERS enum for the `TokenType.token` method."""
        for c in "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ":
            self.assertTrue(TokenType.token(c) is TokenType.CHARACTERS)

    def test_token_delimiters(self):
        """Test the DELIMITERS enum for the `TokenType.token` method."""
        self.assertTrue(TokenType.token(',') is TokenType.DELIMITERS)

    def test_token_numbers(self):
        """Test the NUMBERS enum for the `TokenType.token` method."""
        for n in "0123456789":
            self.assertTrue(TokenType.token(n) is TokenType.NUMBERS)

    def test_token_operators(self):
        """Test the OPERATORS enum for the `TokenType.token` method."""
        for o in "$#[]":
            self.assertTrue(TokenType.token(o) is TokenType.OPERATORS)

    def test_token_comments(self):
        """Test the COMMENTS enum for the `TokenType.token` method."""
        self.assertTrue(TokenType.token(';') is TokenType.COMMENTS)
