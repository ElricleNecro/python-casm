# coding: utf-8

"""Testing the StateMachine class.

:author: Guillaume Plum <guillaume.plum@zaclys.net>
"""


from unittest import TestCase

from casm.ast import ASTNode, Parameter
from casm.compiler import StateMachine
from casm.token import Token


class StateMachineTest(TestCase):
    """Unit test for the Parameter dataclass."""

    def test_parse(self):
        """Test the `parse` method for the `StateMachine` class."""
        answer = ASTNode("add", [Parameter(value="3", is_imm=False, is_hex=False), Parameter(value="4", is_imm=True, is_hex=False)])
        machine = StateMachine()
        ast = machine.parse(
            map(Token, "add 3, #4")
        )

        self.assertEqual(len(ast), 1)
        self.assertEqual(ast[0], answer)

    def test_parse2(self):
        """Test the `parse` method for the `StateMachine` class, with new line."""
        answer = ASTNode("add", [Parameter(value="3", is_imm=False, is_hex=False), Parameter(value="4", is_imm=True, is_hex=False)])
        machine = StateMachine()
        ast = machine.parse(
            map(Token, "add 3, #4\n")
        )

        self.assertEqual(len(ast), 1)
        self.assertEqual(ast[0], answer)

    def test_parse3(self):
        """Test the `parse` method for the `StateMachine` class, with empty line."""
        answers = [
            ASTNode("add", [Parameter(value="3", is_imm=False, is_hex=False), Parameter(value="4", is_imm=True, is_hex=False)]),
            ASTNode("sub", [Parameter(value="5", is_imm=False, is_hex=False), Parameter(value="6", is_imm=True, is_hex=False)]),
        ]
        machine = StateMachine()
        ast = machine.parse(
            map(Token, "add 3, #4\n\nsub 5, #6\n")
        )

        self.assertEqual(len(ast), 2)
        for a, answer in zip(ast, answers):
            self.assertEqual(a, answer)
